
import datetime

class Task(object):
    def __init__(self, taskString=None):
        self.completed = False
        self.dtCompleted = None
        self.priority = None
        self.dtCreated = datetime.date.today()
        self.text = ''
        self.projects = list()
        self.contexts = list()
        if taskString:
            self.dtCreated = None
            self.fromString(taskString)

    def fromString(self, s):
        """parse task from string (s) and populate instance vars"""
        pass

    def toString(self):
        """format object in todo.txt format and return string"""
        pass
