# tests for Task class
from __future__ import absolute_import

import datetime

from task import Task

base = {
        'completed' :False,
        'dtCompleted' :None,
        'priority' :None,
        'dtCreated' :datetime.date.today(),
        'text' :'',
        'projects' :[],
        'contexts' :[],
        'fromString': None,
        'toString': None
        }

def test_TaskInstantiation_noarg():
    """verify that the task exposes the proper attributes and defaults"""
    task = Task()
    assert task.completed == False
    assert task.dtCompleted == None
    assert task.priority == None
    assert task.dtCreated == datetime.date.today()
    assert task.text == ''
    assert task.projects == []
    assert task.contexts == []

def test_TaskAttrs():
    task = Task()
    for a in dir(task):
        if not a.startswith('__'):  #skip dunder private methods/attrs
            assert a in base

def test_TaskAttrsInverse():
    task = Task()
    #verify the reverse
    for a in base:
        assert a in dir(task)

def test_TaskInitFullCompleted():
    #[*complete_flag* *completed_date* ][(*pri*) ][*creation_date* ]Task specific text on one line with both [*project* ] and [*context* ]intermingled with in the task text
    s = "x 2012-03-20 (A) 2012-03-20 My Lovely task +shamoo @home"
    task = Task(s)
    assert task.completed == True
    assert task.dtCompleted == datetime.date(2012,3,20)
    assert task.priority == 'A'
    assert task.dtCreated == datetime.date(2012,3,20)
    assert task.text == 'My Lovely task +shamoo @home'
    assert task.projects == ['+shamoo']
    assert task.contexts == ['@home']

def test_TaskInitFullCompleted3Projects():
    #[*complete_flag* *completed_date* ][(*pri*) ][*creation_date* ]Task specific text on one line with both [*project* ] and [*context* ]intermingled with in the task text
    s = "x 2012-03-20 (A) 2012-03-20 My Lovely task +shamoo +fulcrum +redstone @home"
    task = Task(s)
    assert task.completed == True
    assert task.dtCompleted == datetime.date(2012,3,20)
    assert task.priority == 'A'
    assert task.dtCreated == datetime.date(2012,3,20)
    assert task.text == 'My Lovely task +shamoo +fulcrum +redstone @home'
    assert task.projects == ['+shamoo','+fulcrum','+redstone']
    assert task.contexts == ['@home']

def test_TaskInitFullCompleted3Contexts():
    #[*complete_flag* *completed_date* ][(*pri*) ][*creation_date* ]Task specific text on one line with both [*project* ] and [*context* ]intermingled with in the task text
    s = "x 2012-03-20 (A) 2012-03-20 My Lovely task +shamoo @home @work @sleep"
    task = Task(s)
    assert task.completed == True
    assert task.dtCompleted == datetime.date(2012,3,20)
    assert task.priority == 'A'
    assert task.dtCreated == datetime.date(2012,3,20)
    assert task.text == 'My Lovely task +shamoo @home @work @sleep'
    assert task.projects == ['+shamoo']
    assert task.contexts == ['@home','@work','@sleep']

def test_TaskInitFullCompleted3Projects3Contexts():
    #[*complete_flag* *completed_date* ][(*pri*) ][*creation_date* ]Task specific text on one line with both [*project* ] and [*context* ]intermingled with in the task text
    s = "x 2012-03-20 (A) 2012-03-20 My @home Lovely +shamoo task +fulcrum +redstone @work @sleep"
    task = Task(s)
    assert task.completed == True
    assert task.dtCompleted == datetime.date(2012,3,20)
    assert task.priority == 'A'
    assert task.dtCreated == datetime.date(2012,3,20)
    assert task.text == 'My @home Lovely +shamoo task +fulcrum +redstone @work @sleep'
    assert task.projects == ['+shamoo','+fulcrum','+redstone']
    assert task.contexts == ['@home','@work','@sleep']

def test_TaskInitFullUncompleted():
    #[*complete_flag* *completed_date* ][(*pri*) ][*creation_date* ]Task specific text on one line with both [*project* ] and [*context* ]intermingled with in the task text
    s = "(A) 2012-03-20 My Lovely task +shamoo @home"
    task = Task(s)
    assert task.completed == False
    assert task.dtCompleted == None
    assert task.priority == 'A'
    assert task.dtCreated == datetime.date(2012,3,20)
    assert task.text == 'My Lovely task +shamoo @home'
    assert task.projects == ['+shamoo']
    assert task.contexts == ['@home']


def test_TaskInitPartialUncompleted():
    #[*complete_flag* *completed_date* ][(*pri*) ][*creation_date* ]Task specific text on one line with both [*project* ] and [*context* ]intermingled with in the task text
    s = "(A) My Lovely task +shamoo @home"
    task = Task(s)
    assert task.completed == False
    assert task.dtCompleted == None
    assert task.priority == 'A'
    assert task.dtCreated == None
    assert task.text == 'My Lovely task +shamoo @home'
    assert task.projects == ['+shamoo']
    assert task.contexts == ['@home']

def test_TaskInitPartial2Uncompleted():
    #[*complete_flag* *completed_date* ][(*pri*) ][*creation_date* ]Task specific text on one line with both [*project* ] and [*context* ]intermingled with in the task text
    s = "2012-03-20 My Lovely task +shamoo @home"
    task = Task(s)
    assert task.completed == False
    assert task.dtCompleted == None
    assert task.priority == None
    assert task.dtCreated == datetime.date(2012,3,20)
    assert task.text == 'My Lovely task +shamoo @home'
    assert task.projects == ['+shamoo']
    assert task.contexts == ['@home']

def test_TaskInitPartial3Uncompleted():
    #[*complete_flag* *completed_date* ][(*pri*) ][*creation_date* ]Task specific text on one line with both [*project* ] and [*context* ]intermingled with in the task text
    s = "My Lovely task +shamoo @home"
    task = Task(s)
    assert task.completed == False
    assert task.dtCompleted == None
    assert task.priority == None
    assert task.dtCreated == None
    assert task.text == 'My Lovely task +shamoo @home'
    assert task.projects == ['+shamoo']
    assert task.contexts == ['@home']

def test_TaskInitPartial3UncompletedNoProjects():
    #[*complete_flag* *completed_date* ][(*pri*) ][*creation_date* ]Task specific text on one line with both [*project* ] and [*context* ]intermingled with in the task text
    s = "My Lovely task @home"
    task = Task(s)
    assert task.completed == False
    assert task.dtCompleted == None
    assert task.priority == None
    assert task.dtCreated == None
    assert task.text == 'My Lovely task @home'
    assert task.projects == []
    assert task.contexts == ['@home']

def test_TaskInitPartial3UncompletedNoContexts():
    #[*complete_flag* *completed_date* ][(*pri*) ][*creation_date* ]Task specific text on one line with both [*project* ] and [*context* ]intermingled with in the task text
    s = "My Lovely task +shamoo"
    task = Task(s)
    assert task.completed == False
    assert task.dtCompleted == None
    assert task.priority == None
    assert task.dtCreated == None
    assert task.text == 'My Lovely task +shamoo'
    assert task.projects == ['+shamoo']
    assert task.contexts == []

def test_TaskInitPartial3UncompletedNoProjectsNoContexts():
    #[*complete_flag* *completed_date* ][(*pri*) ][*creation_date* ]Task specific text on one line with both [*project* ] and [*context* ]intermingled with in the task text
    s = "My Lovely task"
    task = Task(s)
    assert task.completed == False
    assert task.dtCompleted == None
    assert task.priority == None
    assert task.dtCreated == None
    assert task.text == 'My Lovely task'
    assert task.projects == []
    assert task.contexts == []

def test_TaskInitPartial3UncompletedNoProjectsNoContextsTricky():
    #[*complete_flag* *completed_date* ][(*pri*) ][*creation_date* ]Task specific text on one line with both [*project* ] and [*context* ]intermingled with in the task text
    s = "x marks the spot, +HideTreasure @deserted_island"
    task = Task(s)
    assert task.completed == False
    assert task.dtCompleted == None
    assert task.priority == None
    assert task.dtCreated == None
    assert task.text == 'x marks the spot, +HideTreasure @deserted_island'
    assert task.projects == ['+HideTreasure',]
    assert task.contexts == ['@deserted_island',]